Affiliate module

PREREQUISITES


OVERVIEW

This modules allows site owners to issue affiliate links to their
partners.  When anonymous users follow these links, the clicks are
counted by affiliate, day, and optional keyword.  When referred users
sign up for an account, the account is associated with that affiliate.

Reporting is currently limited to showing total clicks, signups, and
sales (via the E-Commerce package) to referred users, and a list of
all users referred by each Affiliate.  The module is designed to allow
more detailed reportined to be added.

INSTALLATION

- Activate the module as usual
- Grant 'administer affiliates' permission as needed

USAGE

- Affiliates are no longer a separate entity, they are just users that
  are Affiliate enabled.  To enable a user as an Affiliate, visit
  admin > affiliates, select a user from the drop-down, press Edit,
  and click Enabled.

- Affiliate links are any site url followed by "?a=<uid>".  You can
  also specify an arbitrary keyword with "&k=<keyword>".  The link
  will redirect to the same link with affiliate query removed.

- If the <uid> is enabled as an Affiliate, raw clicks to Affiliate
  links and user signups after clicking on it are counted and reported
  on the admin page.  They are tracked by keyword and day so detailed
  reporting will be possible later, but I have not written that code
  yet.

- Each user's Referrer (the Affiliate that reported them) is shown on
  their Account Information page and can be changed to any other
  Affiliate.

- Tracking is only performed within the current session.  Eventually
  there will be an option to use a cookie, but I have not written that
  code yet.

TO DO

- Track referrals with cookies, not just within a session.
- Report on raw & unique clicks, signups, and sales by time period.
- Affiliates can see reports of their affiliate activity
- Affiliates can earn commissions based on sales to referred users.
- Affiliates sessions allow automatic role_discount for ecomerce purchases
- Affiliates based links allow for affiliate theme to be presented

CREDITS

This module is derived from the Affiliate module for Drupal 4.5,
written by Moshe Weitzman and sponsored by Share New York, an
innovative online music sharing web site - http://sharenewyork.com.

AUTHOR

Barry Jaspan
barry at jaspan dot org

Maintainer 

Thierry Gu�gan (thierry_gd) 